#include <vector>
#include <stdio.h>
#include <string>
#include <cstring>

#include "objLoader.h"



bool loadObject(const char* path, std::vector<Vertex>& out_vertices)

 {

	std::vector< unsigned int > vertexIndices;
	std::vector <glm::vec3> temp_Vertices;

	FILE* file = fopen(path, "r");
	if (file == NULL)
	{

		printf("Impossible to open file.\n");
		return false;

	}
	while (1) {

		char lineHeader[128];
		//read the first word 
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break;

		if (strcmp(lineHeader, "v") == 0) {

			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_Vertices.push_back(vertex);


		}

		else if (strcmp(lineHeader, "f") == 0)
		{

			std::string vertex1, vertex2, vertex3;
			unsigned int outVertexIndex[3];
			int matches = fscanf(file, "%d/%*d/%*d %d/%*d/%*d %d/%*d/%*d\n", &outVertexIndex[0], &outVertexIndex[1], &outVertexIndex[2]);

			if (matches != 3)
			{

				printf("File cannot be read. Matched %d lines\n", matches);
				fclose(file);
				return false;

			}

			vertexIndices.push_back(outVertexIndex[0]);
			vertexIndices.push_back(outVertexIndex[1]);
			vertexIndices.push_back(outVertexIndex[2]);

		}
		else
		{

			//Code to take the rest of the file
			char finishBuffer[1000];
			fgets(finishBuffer, 1000, file);

		}
	}


	
		//For each vertex of each triangle
		for (unsigned int i = 0; i < vertexIndices.size(); i++)
		{

			//get the indices of it's attribute

			unsigned int vertexIndex = vertexIndices[i];

			//get the attributes of the indices 

			Vertex outVertex;

			outVertex.Position = temp_Vertices[vertexIndex - 1];

			//put the attributes in buffers

			outVertex.Color = glm::vec4(1.0f);

			out_vertices.push_back(outVertex);


		}

		fclose(file);
		return true;
	}


