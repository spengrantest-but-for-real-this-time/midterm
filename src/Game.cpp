#include "Game.h"

#include <stdexcept>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"

#include "cereal/cereal.hpp"

#include <fmod.hpp>
#include <fmod_common.h>
#include <fmod_errors.h>

#include "objLoader.h"

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>


Ball thing;
static bool isOrtho = false;
bool keyPress = false;
std::vector<CollisionBodies> coll;

Game::Game() :
	myWindow(nullptr),
	myWindowTitle("Game"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{ }

Game::~Game() { }

void GlfwWindowResizedCallBack(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void Game::Run()
{
	Initialize();
	InitImGui();

	LoadContent();

	FMOD::System* sys;
	FMOD::System_Create(&sys);

	sys->close();
	sys->release();

	static float prevFrame = glfwGetTime();

	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows (clicks, key presses, closing, all that)
		glfwPollEvents();



		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;

		Update(deltaTime);
		Draw(deltaTime);

		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();

		// Present our image to windows
		glfwSwapBuffers(myWindow);

		prevFrame = thisFrame;
	}

	UnloadContent();

	ShutdownImGui();
	Shutdown();
}

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}

	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

	// Create a new GLFW window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);

	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);

	glfwSetWindowUserPointer(myWindow, this);

	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallBack);

	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}
}

void Game::Shutdown() {
	glfwTerminate();
}

void Game::LoadContent() {

	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(10, 0, 0));
	myCamera->LookAt(glm::vec3(0));
	myCamera->Projection = glm::perspective(glm::radians(90.0f), 1.0f, 0.01f, 1000.0f);

	////Read our .obj file
	std::vector< glm::vec3 > vertixes;
	std::vector< glm::vec2 > uvs;
	std::vector< glm::vec3 > normals; // Won't be used at the moment.
	std::vector < Vertex > objVerts;


	//Create our 4 vertices
	Vertex vertices_paddle[4] = {

		//        Position          Colour
		{{ 0.0f, -1.5f, 0.5f}, {1.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f, -1.5f, 0.0f}, {1.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f,  1.5f, 0.5f}, {1.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f,  1.5f, 0.0f}, {1.0f, 0.0f, 0.0f, 1.0f }},

	};

	//create our indices 
	uint32_t indices_paddle[6] = {

		0, 1, 2,
		2, 1, 3

	};

	//Create our 4 vertices
	Vertex vertices_block1[4] = {

		//        Position          Colour
		{{ 0.0f, -0.5f, 0.5f}, {0.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f, -0.5f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f,  0.5f, 0.5f}, {0.0f, 0.0f, 0.0f, 1.0f }},
		{{ 0.0f,  0.5f, 0.0f}, {0.0f, 0.0f, 0.0f, 1.0f }},

	};

	//create our indices 
	uint32_t indices_block1[6] = {

		0, 1, 2,
		2, 1, 3

	};

	//Create our 4 vertices
	Vertex vertices_blockStrong1[4] = {

		//        Position          Colour
		{{ 0.0f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f }},
		{{ 0.0f, -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f, 1.0f }},
		{{ 0.0f,  0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f }},
		{{ 0.0f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f, 1.0f }},

	};

	//create our indices 
	uint32_t indices_blockStrong1[6] = {

		0, 1, 2,
		2, 1, 3

	};


	//create the vertices & indices for the ball

	Vertex vertices_ball[4] =
	{
		{{ 0.0f, 0.0f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f }},

		{{ 0.0f,  0.5f, 0.0f}, {0.0f, 0.0f, 1.0f, 1.0f }},

		{{ 0.0f,  0.0f, -0.5f}, {0.0f, 0.0f, 1.0f, 1.0f }},
		{{ 0.0f,  -0.5f, 0.0f}, {0.0f, 0.0f, 1.0f, 1.0f }},


	};

	uint32_t indices_ball[6] = {
		0, 1, 2,
		2, 3, 0

	};


	//create a new mech from the data
	paddle = std::make_shared<Mesh>(vertices_paddle, 4, indices_paddle, 6);

	block1 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block2 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block3 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block4 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block5 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block6 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block7 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block8 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block9 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block10 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block11 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block12 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block13 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block14 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block15 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block16 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block17 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block18 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block19 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block20 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block21 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block22 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block23 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);

	block24 = std::make_shared<Mesh>(vertices_block1, 4, indices_block1, 6);


	strongBlock1 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock2 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock3 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock4 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock5 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock6 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock7 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock8 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock9 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock10 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock11 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	strongBlock12 = std::make_shared<Mesh>(vertices_blockStrong1, 4, indices_blockStrong1, 6);

	ball = std::make_shared<Mesh>(vertices_ball, 9, indices_ball, 24);


	//Create and Compile Shader
	myShader = std::make_shared<Shader>();
	myShader->Load("passthrough.vs", "passthrough.fs");

	// Our models transformation matrix
	paddleTransform = glm::mat4(0.0f);

	blockTransform1 = glm::mat4(1.0f);

	blockTransform2 = glm::mat4(1.0f);

	blockTransform3 = glm::mat4(1.0f);

	blockTransform4 = glm::mat4(1.0f);

	blockTransform5 = glm::mat4(1.0f);

	blockTransform6 = glm::mat4(1.0f);

	blockTransform7 = glm::mat4(1.0f);

	blockTransform8 = glm::mat4(1.0f);

	blockTransform9 = glm::mat4(1.0f);

	blockTransform10 = glm::mat4(1.0f);

	blockTransform11 = glm::mat4(1.0f);

	blockTransform12 = glm::mat4(1.0f);

	blockTransform13 = glm::mat4(1.0f);

	blockTransform14 = glm::mat4(1.0f);

	blockTransform15 = glm::mat4(1.0f);

	blockTransform16 = glm::mat4(1.0f);

	blockTransform17 = glm::mat4(1.0f);

	blockTransform18 = glm::mat4(1.0f);

	blockTransform19 = glm::mat4(1.0f);

	blockTransform20 = glm::mat4(1.0f);

	blockTransform21 = glm::mat4(1.0f);

	blockTransform22 = glm::mat4(1.0f);

	blockTransform23 = glm::mat4(1.0f);

	blockTransform24 = glm::mat4(1.0f);

	strongBlockTransform1 = glm::mat4(1.0f);

	strongBlockTransform2 = glm::mat4(1.0f);

	strongBlockTransform3 = glm::mat4(1.0f);

	strongBlockTransform4 = glm::mat4(1.0f);

	strongBlockTransform5 = glm::mat4(1.0f);

	strongBlockTransform6 = glm::mat4(1.0f);

	strongBlockTransform7 = glm::mat4(1.0f);

	strongBlockTransform8 = glm::mat4(1.0f);

	strongBlockTransform9 = glm::mat4(1.0f);

	strongBlockTransform10 = glm::mat4(1.0f);

	strongBlockTransform11 = glm::mat4(1.0f);

	strongBlockTransform12 = glm::mat4(1.0f);

	ballTransform = glm::mat4(1.0f);

	blockTransform1 = glm::translate(blockTransform1, glm::vec3(20, 20, 20));

	blockTransform2 = glm::translate(blockTransform2, glm::vec3(20, 20, 20));

	blockTransform3 = glm::translate(blockTransform3, glm::vec3(20, 20, 20));

	blockTransform4 = glm::translate(blockTransform4, glm::vec3(20, 20, 20));

	blockTransform5 = glm::translate(blockTransform5, glm::vec3(20, 20, 20));

	blockTransform6 = glm::translate(blockTransform6, glm::vec3(20, 20, 20));

	blockTransform7 = glm::translate(blockTransform7, glm::vec3(20, 20, 20));

	blockTransform8 = glm::translate(blockTransform8, glm::vec3(20, 20, 20));

	blockTransform9 = glm::translate(blockTransform9, glm::vec3(20, 20, 20));

	blockTransform10 = glm::translate(blockTransform10, glm::vec3(20, 20, 20));

	blockTransform11 = glm::translate(blockTransform11, glm::vec3(20, 20, 20));

	blockTransform12 = glm::translate(blockTransform12, glm::vec3(20, 20, 20));

	blockTransform13 = glm::translate(blockTransform13, glm::vec3(0, -8, 7));

	blockTransform14 = glm::translate(blockTransform14, glm::vec3(0, -6.5, 7));

	blockTransform15 = glm::translate(blockTransform15, glm::vec3(0, -5, 7));

	blockTransform16 = glm::translate(blockTransform16, glm::vec3(0, -3.5, 7));

	blockTransform17 = glm::translate(blockTransform17, glm::vec3(0, -2, 7));

	blockTransform18 = glm::translate(blockTransform18, glm::vec3(0, -0.5, 7));

	blockTransform19 = glm::translate(blockTransform19, glm::vec3(0, 1, 7));

	blockTransform20 = glm::translate(blockTransform20, glm::vec3(0, 2.5, 7));

	blockTransform21 = glm::translate(blockTransform21, glm::vec3(0, 4, 7));

	blockTransform22 = glm::translate(blockTransform22, glm::vec3(0, 5.5, 7));

	blockTransform23 = glm::translate(blockTransform23, glm::vec3(0, 7, 7));

	blockTransform24 = glm::translate(blockTransform24, glm::vec3(0, 8.5, 7));

	strongBlockTransform1 = glm::translate(strongBlockTransform1, glm::vec3(0, -8, 8));

	strongBlockTransform2 = glm::translate(strongBlockTransform2, glm::vec3(0, -6.5, 8));

	strongBlockTransform3 = glm::translate(strongBlockTransform3, glm::vec3(0, -5, 8));

	strongBlockTransform4 = glm::translate(strongBlockTransform4, glm::vec3(0, -3.5, 8));

	strongBlockTransform5 = glm::translate(strongBlockTransform5, glm::vec3(0, -2, 8));

	strongBlockTransform6 = glm::translate(strongBlockTransform6, glm::vec3(0, -0.5, 8));

	strongBlockTransform7 = glm::translate(strongBlockTransform7, glm::vec3(0, 1, 8));

	strongBlockTransform8 = glm::translate(strongBlockTransform8, glm::vec3(0, 2.5, 8));

	strongBlockTransform9 = glm::translate(strongBlockTransform9, glm::vec3(0, 4, 8));

	strongBlockTransform10 = glm::translate(strongBlockTransform10, glm::vec3(0, 5.5, 8));

	strongBlockTransform11 = glm::translate(strongBlockTransform11, glm::vec3(0, 7, 8));

	strongBlockTransform12 = glm::translate(strongBlockTransform12, glm::vec3(0, 8.5, 8));

	glm::vec3 blockTransformAfter1;
	glm::vec3 blockTransformAfter2;
	glm::vec3 blockTransformAfter3;
	glm::vec3 blockTransformAfter4;
	glm::vec3 blockTransformAfter5;
	glm::vec3 blockTransformAfter6;
	glm::vec3 blockTransformAfter7;
	glm::vec3 blockTransformAfter8;
	glm::vec3 blockTransformAfter9;
	glm::vec3 blockTransformAfter10;
	glm::vec3 blockTransformAfter11;
	glm::vec3 blockTransformAfter12;
	glm::vec3 blockTransformAfter13;
	glm::vec3 blockTransformAfter14;
	glm::vec3 blockTransformAfter15;
	glm::vec3 blockTransformAfter16;
	glm::vec3 blockTransformAfter17;
	glm::vec3 blockTransformAfter18;
	glm::vec3 blockTransformAfter19;
	glm::vec3 blockTransformAfter20;
	glm::vec3 blockTransformAfter21;
	glm::vec3 blockTransformAfter22;
	glm::vec3 blockTransformAfter23;
	glm::vec3 blockTransformAfter24;

	glm::vec3 strongBlockTransformAfter1;
	glm::vec3 strongBlockTransformAfter2;
	glm::vec3 strongBlockTransformAfter3;
	glm::vec3 strongBlockTransformAfter4;
	glm::vec3 strongBlockTransformAfter5;
	glm::vec3 strongBlockTransformAfter6;
	glm::vec3 strongBlockTransformAfter7;
	glm::vec3 strongBlockTransformAfter8;
	glm::vec3 strongBlockTransformAfter9;
	glm::vec3 strongBlockTransformAfter10;
	glm::vec3 strongBlockTransformAfter11;
	glm::vec3 strongBlockTransformAfter12;
	


	paddleCollide.SetType(2);
	paddleCollide.SetLength(1.0f);
	paddleCollide.SetWidth(3.0f);

	CollisionBodies  blockCollide1(2, 0.0f, 0.5f, 1.0f, blockTransformAfter1 = glm::vec3(blockTransform1 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide2(2, 0.0f, 0.5f, 1.0f, blockTransformAfter2 = glm::vec3(blockTransform2 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide3(2, 0.0f, 0.5f, 1.0f, blockTransformAfter3 = glm::vec3(blockTransform3 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide4(2, 0.0f, 0.5f, 1.0f, blockTransformAfter4 = glm::vec3(blockTransform4 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide5(2, 0.0f, 0.5f, 1.0f, blockTransformAfter5 = glm::vec3(blockTransform5 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide6(2, 0.0f, 0.5f, 1.0f, blockTransformAfter6 = glm::vec3(blockTransform6 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide7(2, 0.0f, 0.5f, 1.0f, blockTransformAfter7 = glm::vec3(blockTransform7 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide8(2, 0.0f, 0.5f, 1.0f, blockTransformAfter8 = glm::vec3(blockTransform8 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies  blockCollide9(2, 0.0f, 0.5f, 1.0f, blockTransformAfter9 = glm::vec3(blockTransform9 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies blockCollide10(2, 0.0f, 0.5f, 1.0f, blockTransformAfter10 = glm::vec3(blockTransform10 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies blockCollide11(2, 0.0f, 0.5f, 1.0f, blockTransformAfter11 = glm::vec3(blockTransform11 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies blockCollide12(2, 0.0f, 0.5f, 1.0f, blockTransformAfter12 = glm::vec3(blockTransform12 * glm::vec4(1, 1, 1, 0.0f)));
	CollisionBodies blockCollide13(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -8, 7));
	CollisionBodies blockCollide14(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -6.5, 7));
	CollisionBodies blockCollide15(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -5, 7));
	CollisionBodies blockCollide16(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -3.5, 7));
	CollisionBodies blockCollide17(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -2, 7));
	CollisionBodies blockCollide18(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -0.5, 7));
	CollisionBodies blockCollide19(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 1, 7));
	CollisionBodies blockCollide20(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 2.5, 7));
	CollisionBodies blockCollide21(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 4, 7));
	CollisionBodies blockCollide22(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 5.5, 7));
	CollisionBodies blockCollide23(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 7, 7));
	CollisionBodies blockCollide24(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 8.5, 7));

	CollisionBodies  strongBlockCollide1(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -8, 8));
	CollisionBodies  strongBlockCollide2(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -6.5, 8));
	CollisionBodies  strongBlockCollide3(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -5, 8));
	CollisionBodies  strongBlockCollide4(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -3.5, 8));
	CollisionBodies  strongBlockCollide5(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -2, 8));
	CollisionBodies  strongBlockCollide6(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, -0.5, 8));
	CollisionBodies  strongBlockCollide7(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 1, 8));
	CollisionBodies  strongBlockCollide8(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 2.5, 8));
	CollisionBodies  strongBlockCollide9(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 4, 8));
	CollisionBodies  strongBlockCollide10(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 5.5, 8));
	CollisionBodies  strongBlockCollide11(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 7, 8));
	CollisionBodies  strongBlockCollide12(2, 0.0f, 0.5f, 1.0f, glm::vec3(0, 8.5, 8));

	ballCollide.SetType(1);
	ballCollide.SetRadius(0.5f);

	paddleCollide.SetPosition(glm::vec3(0,-8,0));
	ballCollide.SetPosition(glm::vec3(0,0,0));
	

	coll.push_back(paddleCollide);
	/*coll.push_back(blockCollide1);
	coll.push_back(blockCollide2);
	coll.push_back(blockCollide3);
	coll.push_back(blockCollide4);
	coll.push_back(blockCollide5);
	coll.push_back(blockCollide6);
	coll.push_back(blockCollide7);
	coll.push_back(blockCollide8);
	coll.push_back(blockCollide9);
	coll.push_back(blockCollide10);
	coll.push_back(blockCollide11);
	coll.push_back(blockCollide12);*/
	coll.push_back(blockCollide13);
	coll.push_back(blockCollide14);
	coll.push_back(blockCollide15);
	coll.push_back(blockCollide16);
	coll.push_back(blockCollide17);
	coll.push_back(blockCollide18);
	coll.push_back(blockCollide19);
	coll.push_back(blockCollide20);
	coll.push_back(blockCollide21);
	coll.push_back(blockCollide22);
	coll.push_back(blockCollide23);
	coll.push_back(blockCollide24);
	coll.push_back(strongBlockCollide1);
	coll.push_back(strongBlockCollide2);
	coll.push_back(strongBlockCollide3);
	coll.push_back(strongBlockCollide4);
	coll.push_back(strongBlockCollide5);
	coll.push_back(strongBlockCollide6);
	coll.push_back(strongBlockCollide7);
	coll.push_back(strongBlockCollide8);
	coll.push_back(strongBlockCollide9);
	coll.push_back(strongBlockCollide10);
	coll.push_back(strongBlockCollide11);
	coll.push_back(strongBlockCollide12);
}

void Game::UnloadContent() {

}

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();
	// Gets our ImGUI input/output 
	ImGuiIO& io = ImGui::GetIO();
	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);
	ImGui_ImplOpenGL3_Init("#version 410");

	// Dark mode FTW
	ImGui::StyleColorsDark();

	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();
	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}

void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);

	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Update(float deltaTime) {


	//Object movement variables
	static glm::vec3 objMovement = glm::vec3(0.0f);


	static glm::vec3 objMovementTrue = objMovement + glm::vec3(0.0f, 0.0f, -8.0f);

	static glm::vec3 ballMovement = glm::vec3(0.0f);

	float objSpeed = 6.0f;
	float objRotSpeed = 45.0;


	//object Movement Variables
	if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
		objMovementTrue.y -= objSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
		objMovementTrue.y += objSpeed * deltaTime;

	ballMovement.y += ballSpeed * deltaTime;
	ballMovement.z += ballSpeed * deltaTime;

	//Move our transformation matrix each frame

	paddleTransform = (glm::translate(glm::mat4(1.0f), objMovementTrue));

	ballTransform = (glm::translate(glm::mat4(1.0f), ballMovement));



	//update position of the paddle collide box
	paddleCollide.SetPosition(glm::vec3(0, (paddleCollide.GetPosition().y + objMovementTrue.y), paddleCollide.GetPosition().z));
	//update position of the ball collide box
	ballCollide.SetPosition(glm::vec3(0, (ballCollide.GetPosition().y + ballMovement.y), (ballCollide.GetPosition().z + ballMovement.z)));
	//check for collisions
		for (int i = 0; i < coll.size(); i++) {
		if (ballCollide.Collision(coll[i]) == true) {
			ballSpeed = -1*ballSpeed;
			std::cout << "hello " << i <<std::endl;
			std::cout << "block " << coll[i].GetPosition().y << " " << coll[i].GetPosition().z << std::endl;
			std::cout << "ball " << ballCollide.GetPosition().y << " " << ballCollide.GetPosition().z << std::endl;
			//checks the ball isnt colliding with the panel
			if (i != 1) {
				//yeet the aprpopriate block in here
				//didnt think far enough of how to attach the mesh to the collision block, oops
				//but this will yeet the collision box so it wont collide again
				coll[i].SetPosition(glm::vec3(20, 2000000000, 20));
			}
		}
	}


}

void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT);

	myShader->Bind();
	// Update our uniform
	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * paddleTransform);
	paddle->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform1);
	block1->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform2);
	block2->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform3);
	block3->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform4);
	block4->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform5);
	block5->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform6);
	block6->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform7);
	block7->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform8);
	block8->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform9);
	block9->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform10);
	block10->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform11);
	block11->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform12);
	block12->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform13);
	block13->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform14);
	block14->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform15);
	block15->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform16);
	block16->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform17);
	block17->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform18);
	block18->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform19);
	block19->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform20);
	block20->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform21);
	block21->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform22);
	block22->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform23);
	block23->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * blockTransform24);
	block24->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform1);
	strongBlock1->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform2);
	strongBlock2->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform3);
	strongBlock3->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform4);
	strongBlock4->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform5);
	strongBlock5->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform6);
	strongBlock6->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform7);
	strongBlock7->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform8);
	strongBlock8->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform9);
	strongBlock9->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform10);
	strongBlock10->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform11);
	strongBlock11->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * strongBlockTransform12);
	strongBlock12->Draw();

	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * ballTransform);
	ball->Draw();


}

void Game::DrawGui(float deltaTime) {
	// Open a new ImGui window
	ImGui::Begin("Test");

	// Draw a color editor
	ImGui::ColorEdit4("Clear Color", &myClearColor[0]);


	// Check if a textbox has changed, and update our window title if it has
	if (ImGui::InputText("Window Title", myWindowTitle, 32)) {
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}

	ImGui::End();

	// Open a second ImGui window
	ImGui::Begin("Debug");
	// Draw a formatted text line
	ImGui::Text("Time: %f", glfwGetTime());
	ImGui::End();
}

