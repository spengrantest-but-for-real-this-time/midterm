#pragma once


#include <glad/glad.h>
#include <memory>
#include <Logging.h>
#include <GLM/glm.hpp>

class Shader {
public:
	typedef std::shared_ptr<Shader> Sptr;

	Shader();
	~Shader();

	void Compile(const char* vs_source, const char* fs_source);

	//Loads the shader program from 2 files
	void Load(const char* vsFile, const char* fsFile);

	void Bind();

	void SetUniform(const char* name, const glm::mat4& value);




private:
	GLuint _CompileShaderPart(const char* source, GLenum type);

	GLuint myShaderHandle;

};

