#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "GLM/glm.hpp"

#include "Mesh.h"
#include "Shader.h"

#include <Camera.h>

#include "Collisions.h"
#include "Ball.h"

class Game {
public:
	Game();
	~Game();

	void Run();



protected:
	void Initialize();
	void Shutdown();

	void LoadContent();
	void UnloadContent();

	void InitImGui();
	void ShutdownImGui();

	void ImGuiNewFrame();
	void ImGuiEndFrame();

	void Update(float deltaTime);
	void Draw(float deltaTime);
	void DrawGui(float deltaTime);

	Camera::Sptr myCamera;

private:
	// Stores the main window that the game is running in
	GLFWwindow* myWindow;
	// Stores the clear color of the game's window
	glm::vec4   myClearColor;
	// Stores the title of the game's window
	char        myWindowTitle[32];



	//A shared pointer to our mesh
	Mesh::Sptr paddle;

	Mesh::Sptr block1;

	Mesh::Sptr block2;

	Mesh::Sptr block3;

	Mesh::Sptr block4;

	Mesh::Sptr block5;

	Mesh::Sptr block6;

	Mesh::Sptr block7;

	Mesh::Sptr block8;

	Mesh::Sptr block9;

	Mesh::Sptr block10;

	Mesh::Sptr block11;

	Mesh::Sptr block12;

	Mesh::Sptr block13;

	Mesh::Sptr block14;

	Mesh::Sptr block15;

	Mesh::Sptr block16;

	Mesh::Sptr block17;

	Mesh::Sptr block18;
	
	Mesh::Sptr block19;

	Mesh::Sptr block20;

	Mesh::Sptr block21;

	Mesh::Sptr block22;

	Mesh::Sptr block23;

	Mesh::Sptr block24;
	
	Mesh::Sptr strongBlock1;
			   
	Mesh::Sptr strongBlock2;
			   
	Mesh::Sptr strongBlock3;
			   
	Mesh::Sptr strongBlock4;
			   
	Mesh::Sptr strongBlock5;
			   
	Mesh::Sptr strongBlock6;
			   
	Mesh::Sptr strongBlock7;
			   
	Mesh::Sptr strongBlock8;
			   
	Mesh::Sptr strongBlock9;
			   
	Mesh::Sptr strongBlock10;
			   
	Mesh::Sptr strongBlock11;
			   
	Mesh::Sptr strongBlock12;
			   
	Mesh::Sptr ball;

	//A shared pointer to our shader
	Shader::Sptr myShader;

	// Our models transformation matrix
	glm::mat4 paddleTransform;


	glm::mat4 blockTransform1;

	glm::mat4 blockTransform2;

	glm::mat4 blockTransform3;

	glm::mat4 blockTransform4;

	glm::mat4 blockTransform5;

	glm::mat4 blockTransform6;

	glm::mat4 blockTransform7;

	glm::mat4 blockTransform8;

	glm::mat4 blockTransform9;

	glm::mat4 blockTransform10;

	glm::mat4 blockTransform11;

	glm::mat4 blockTransform12;

	glm::mat4 blockTransform13;

	glm::mat4 blockTransform14;

	glm::mat4 blockTransform15;

	glm::mat4 blockTransform16;

	glm::mat4 blockTransform17;

	glm::mat4 blockTransform18;

	glm::mat4 blockTransform19;

	glm::mat4 blockTransform20;

	glm::mat4 blockTransform21;

	glm::mat4 blockTransform22;

	glm::mat4 blockTransform23;

	glm::mat4 blockTransform24;

	glm::mat4 strongBlockTransform1;
			  
	glm::mat4 strongBlockTransform2;
			  
	glm::mat4 strongBlockTransform3;
			  
	glm::mat4 strongBlockTransform4;
			  
	glm::mat4 strongBlockTransform5;
			  
	glm::mat4 strongBlockTransform6;
			  
	glm::mat4 strongBlockTransform7;
			  
	glm::mat4 strongBlockTransform8;
			  
	glm::mat4 strongBlockTransform9;
			  
	glm::mat4 strongBlockTransform10;
			  
	glm::mat4 strongBlockTransform11;
			  
	glm::mat4 strongBlockTransform12;


	glm::vec3 ballTransformAfter;
	glm::vec3 paddleTransformAfter;


	glm::mat4 ballTransform;

	CollisionBodies ballCollide;
	CollisionBodies paddleCollide;

	float ballSpeed = 4.0f;
};