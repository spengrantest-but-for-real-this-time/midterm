#pragma once
//Kaylyn McCune- 100662337
//Spencer Tester- 100653129

#include <GLM/glm.hpp>

class Ball {
public:
	//constructor/destructor
	Ball();
	~Ball();

	//Get/Set Functions
	glm::vec3 GetPosition();
	void SetPosition(glm::vec3 p);

	glm::vec3 GetVelocity();
	void SetVelocity(glm::vec3 v);

	int GetLife();
	void SetLife(int l);

	//Reverses the velocity vector if the ball bounces
	void Bounce(int side);

	//Reduces the players life by one if the ball goes off screen
	void LoseLife();

	//updates the player's position based on the velocity vector
	void UpdatePosition();

private:
	
	glm::vec3 position;
	glm::vec3 velocity;
	int life;
};