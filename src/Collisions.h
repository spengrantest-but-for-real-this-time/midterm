#pragma once
//Kaylyn McCune- 100662337
//Spencer Tester- 100653129

#include <GLM/glm.hpp>

class CollisionBodies {

public:
	
	//Constructor, Default Constructor, Destructor
	CollisionBodies(int t, float r, float l, float w, glm::vec3 p);
	CollisionBodies();
	~CollisionBodies();

	//Get/Set functions for each variable
	int GetType();
	void SetType(int t);

	float GetRadius();
	void SetRadius(float r);

	float GetLength();
	void SetLength(float l);

	float GetWidth();
	void SetWidth(float w);

	glm::vec3 GetPosition();
	void SetPosition(glm::vec3 p);

	//Collision Detection Algorithm
	bool Collision(CollisionBodies second);

private:
	
	//use a number to store what type of body it is, circular or rectangular
	//1 = circle, 2 = rectangle
	int type;
	
	//store radius for a circle, and length (y) and width (z) for rectangular
	float radius;
	float length;
	float width;

	//Store the position of the body
	glm::vec3 position;

};