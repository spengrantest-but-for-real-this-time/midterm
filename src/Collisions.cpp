//Kaylyn McCune- 100662337
//Spencer Tester- 100653129

#include "Collisions.h"
#include <math.h>

//Constructor, set the specified variables to the inputed values
CollisionBodies::CollisionBodies(int t, float r, float l, float w, glm::vec3 p) {
	
	type = t;
	radius = r;
	length = l;
	width = w;
	position = p;
}

//Default Consturctor, Nothing needs to be set
CollisionBodies::CollisionBodies() {

}

//Destructor, nothing to be destroyed
CollisionBodies::~CollisionBodies() {

}

//Get/Set functions for each variable
//Returns the type of body, 1= circular, 2 = rectangular
int CollisionBodies::GetType() {
	return type;
}

//Sets the type of collision body, 1 = circular, 2= rectangular
void CollisionBodies::SetType(int t) {
	type = t;
}

//Returns the radius of the collision body
float CollisionBodies::GetRadius() {
	return radius;
}

//Sets the radius of the collision body
void CollisionBodies::SetRadius(float r) {
	radius = r;
}

//Returns the length of the collision body
float CollisionBodies::GetLength() {
	return length;
}

//Sets the length of the collision body
void CollisionBodies::SetLength(float l) {
	length = l;
}

//Returns the width of the collision body
float CollisionBodies::GetWidth() {
	return width;
}

//Stes the width of the collision body
void CollisionBodies::SetWidth(float w) {
	width = w;
}

//Returns the position of the collision body
glm::vec3 CollisionBodies::GetPosition() {
	return position;
}

//Sets the position of the collision body
void CollisionBodies::SetPosition(glm::vec3 p) {
	position = p;
}

//Collision Detection Algorithm
bool CollisionBodies::Collision(CollisionBodies second) {
	
	//if/else statement determines what type of collision is taking place
	//if both types are 1, its circle-circle collision
	//if both types are 2, its rectangle-rectangle collision
	//if the types do not match, its circle-rectangle collision
	if (type == 1 && second.GetType() == 1) {
		
		//circle circle

		//add the radius of both collision bodies together
		float ultr = radius + second.GetRadius();

		//find the distance between the 2 bodies
		glm::vec3 space = glm::vec3((second.GetPosition().x - position.x), (second.GetPosition().y - position.y), (second.GetPosition().z - position.z));
		float distance = sqrt((pow(space.x, 2) + pow(space.y, 2) + pow(space.z, 2)));

		//if distance is greater than radius, not colliding
		//if they equal, they touching
		//if radius is greater than distance, they colliding
		if (ultr < distance) {
			return false;
		}
		else if (ultr > distance) {
			return true;
		}
		else {
			return true;
		}

	}
	else if (type == 2 && second.GetType() == 2) {

		//rectangle rectangle
		//needs to be greater than the least x but less than the larger x
		//check if the second rectangle is to the left or the right of the first
		return false;

	}
	else {

		//circle rectangle
		//determine where the square lies in comparison to the circle
		//determine which object is the square
		bool y, z;

		if (type == 2) {
			//first object is the square
			if (position.y < second.GetPosition().y) {
				//square is to the left
				//get leftmost position of circle and rightmost of square
				float i = second.GetPosition().y - second.GetRadius();
				float j = position.y + (length * 0.5);

				//check if the y overlaps
				if (i <= j) {
					y = true;
				}
				else {
					y = false;
				}
			}
			else {
				//square is to the right
				//get rightmost position of circle and leftmost of square
				float i = second.GetPosition().y + second.GetRadius();
				float j = position.y - (length * 0.5);

				//check if the y overlaps
				if (i >= j) {
					y = true;
				}
				else {
					y = false;
				}
			}


			if (position.z > second.GetPosition().z) {
				//square is up
				//get upmost position of circle and downmost of square
				float i = second.GetPosition().z + second.GetRadius();
				float j = position.z - (width * 0.5);

				//check if the z overlaps
				if (i >= j) {
					z = true;
				}
				else {
					z = false;
				}
			}
			else {
				//square is down
				//get downmost position of circle and upmostmost of square
				float i = second.GetPosition().z - second.GetRadius();
				float j = position.z + (length * 0.5);

				//check if the z overlaps
				if (i <= j) {
					z = true;
				}
				else {
					z = false;
				}
			}

			if (y && z) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			//second object is the square
			
			if (second.GetPosition().y < position.y) {
				//square is to the left
				//get leftmost position of circle and rightmost of square
				float i = position.y - radius;
				float j = second.GetPosition().y + (second.GetLength() * 0.5);

				//check if the y overlaps
				if (i <= j) {
					y = true;
				}
				else {
					y = false;
				}
			}
			else {
				//square is to the right
				//get rightmost position of circle and leftmost of square
				float i = position.y + radius;
				float j = second.GetPosition().y - (second.GetLength() * 0.5);

				//check if the y overlaps
				if (i >= j) {
					y = true;
				}
				else {
					y = false;
				}
			}


			if (second.GetPosition().z > position.z) {
				//square is up
				//get upmost position of circle and downmost of square
				float i = position.z + radius;
				float j = second.GetPosition().z - (second.GetWidth() * 0.5);

				//check if the z overlaps
				if (i >= j) {
					z = true;
				}
				else {
					z = false;
				}
			}
			else {
				//square is down
				//get downmost position of circle and upmostmost of square
				float i = position.z - radius;
				float j = second.GetPosition().z + (second.GetLength() * 0.5);

				//check if the z overlaps
				if (i <= j) {
					z = true;
				}
				else {
					z = false;
				}
			}

			if (y && z) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}