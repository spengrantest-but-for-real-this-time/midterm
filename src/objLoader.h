#pragma once


#include <vector> //for std::vector
#include <GLM/glm.hpp> //for vec3 and vec4
#include "Mesh.h"


bool loadObject(const char* path, std::vector<Vertex>& out_vertices);