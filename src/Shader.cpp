#include "Shader.h"
#include <fstream>

Shader::Shader() {
	myShaderHandle = glCreateProgram();
}

Shader::~Shader() {
	glDeleteProgram(myShaderHandle);
}




void Shader::Bind() {
	glUseProgram(myShaderHandle);
}

GLuint Shader::_CompileShaderPart(const char* source, GLenum type) {
	GLuint result = glCreateShader(type);

	//Load in our shader source and compile with it
	glShaderSource(result, 1, &source, NULL);
	glCompileShader(result);

	GLint compileStatus = 0;
	glGetShaderiv(result, GL_COMPILE_STATUS, &compileStatus);

	//If we failed to compile

	if (compileStatus == GL_FALSE)
	{

		//Get the size of the error log
		GLint logSize = 0;
		glGetShaderiv(result, GL_INFO_LOG_LENGTH, &logSize);

		//create a new character buffer for the log
		char* log = new char[logSize];

		//get the log
		glGetShaderInfoLog(result, logSize, &logSize, log);

		//Dump Error Log
		LOG_ERROR("Failed to compile Shader part:\n{}", log);

		//Clean up log memory
		delete[] log;

		//Delete broken Shader Result
		glDeleteShader(result);

		//throw a runtime exception
 		throw std::runtime_error("Failed to Compile Shader Part");
	}
	else {
		LOG_TRACE("Shader Part has been compiled!");
	}

	//return the compiled shader part
	return result;
}

void Shader::Compile(const char* vs_source, const char* fs_source) {
	//compile our two shader programs
	GLuint vs = _CompileShaderPart(vs_source, GL_VERTEX_SHADER);
	GLuint fs = _CompileShaderPart(fs_source, GL_FRAGMENT_SHADER);

	//Attatch our two shaders
	glAttachShader(myShaderHandle, vs);
	glAttachShader(myShaderHandle, fs);

	//Perform Linking
	glLinkProgram(myShaderHandle);

	//Get whether link was successful
	GLint success = 0;
	glGetProgramiv(myShaderHandle, GL_LINK_STATUS, &success);

	//if not we grab a log and throw an exception
	if (success == GL_FALSE)
	{
		//todo: read the log file
		//throw a runtime exception
		throw new std::runtime_error("Failed to link shader program!");
	}
	else {

		LOG_TRACE("Shader has been linked");

	}

	if (success == GL_FALSE) {

		//Get the length of the log
		GLint length = 0;
		glGetProgramiv(myShaderHandle, GL_INFO_LOG_LENGTH, &length);

		if (length > 0) {

			//read the log from openGL
			char* log = new char[length];
			glGetProgramInfoLog(myShaderHandle, length, &length, log);
			LOG_ERROR("Shader failed to link :\n{}", log);
			delete[]log;

		}

		else {

			LOG_ERROR("Shader Failed to Link for Unknown Reasons!");

		}

		//Delete the partial Program
		glDeleteProgram(myShaderHandle);

		//throw a runtime exception
		throw new std::runtime_error("Failed to link shader programs.");

	}


	

}

void Shader::SetUniform(const char* name, const glm::mat4& value) {
	GLint loc = glGetUniformLocation(myShaderHandle, name);
	if (loc != -1) {
		glProgramUniformMatrix4fv(myShaderHandle, loc, 1, false, &value[0][0]);
	}

}

// Reads the entire contents of a file
char* readFile(const char* filename) {
	// Declare and open the file stream
	std::ifstream file;
	file.open(filename, std::ios::binary);
	// Only read if the file is open
	if (file.is_open()) {
		// Get the starting location in the file
		uint64_t fileSize = file.tellg();
		// Seek to the end
		file.seekg(0, std::ios::end);
		// Calculate the file size from end to beginning
		fileSize = (uint64_t)file.tellg() - fileSize;
		// Seek back to the beginning of the file
		file.seekg(0, std::ios::beg);
		// Allocate space for our entire file, +1 byte at the end for null terminator
		char* result = new char[fileSize + 1];
		// Read the entire file to our memory
		file.read(result, fileSize);
		// Make our text null-terminated
		result[fileSize] = '\0';
		// Close the file before returning
		file.close();
		return result;
	}
	// Otherwise, we failed to open our file, throw a runtime error
	else {
		throw std::runtime_error("We cannot open the file!");
	}
}


void Shader::Load(const char* vsFile, const char* fsFile)
{
	// Load in our shaders
	char* vs_source = readFile(vsFile);
	char* fs_source = readFile(fsFile);
	// Compile our program
	Compile(vs_source, fs_source);
	// Clean up our memory
	delete[] fs_source;
	delete[] vs_source;
}
